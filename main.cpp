﻿#include "tobot.hpp"

#include <memory>
#include <coroutine>
#include <unistd.h>

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/address.hpp>
using namespace std;



int main(int argc, char **argv) {
    bool use_nickserv;

    // Check args
    if (argc == 5) {
        use_nickserv = false;
    } else if (argc == 7) {
        use_nickserv = true;
    } else {
        std::cout << "Usage: " << argv[0] << " <ip> <port> <name> <, seperated channels> [<nickserv name> <nickserv password>] {channels...}" << std::endl;
        return EXIT_FAILURE;
    }

    // Get args
    auto ip = argv[1],
         port = argv[2],
         name = argv[3],
         channels = argv[4],
         ns_name = argv[5],
         ns_password = argv[6];

    // Create IO service
    boost::asio::io_service io;

    // Set up client
    static auto client = std::make_shared<MyClient>(io, ChatFuse::IRC::Settings{
                                                 .ip = boost::asio::ip::address::from_string(ip),
                                                 .port = static_cast<unsigned short>(std::atoi(port)),
                                                 .name = name
                                             });
    if (use_nickserv) {
        client->use_nickservauth(ns_name, ns_password);
    }
    {
        std::string_view iteratable_channels = channels;
        std::string channel;
        for (const char c : iteratable_channels) {
            if (c != ',') {
                channel.push_back(c);
            } else {
                client->add_channel(std::move(channel), false);
                channel = std::string();
            }
        }
        if (!channel.empty()) {
            client->add_channel(std::move(channel), false);
        }
    }
    client->detach();

    if (use_nickserv) {
        // Erase nickserv password from argv (so it's no longer visible in /proc/{pid}/cmdline)
        memset(ns_password, 0, strlen(ns_password));
    }

    // RUN!!!
    io.run();
    return EXIT_SUCCESS;
}
