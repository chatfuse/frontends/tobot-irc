#include <string>
#include <tuple>
#include <exception>
#include <memory>
#include <optional>
#include <unordered_map>

#include <logger.hpp>
#include <justlm.hpp>
#include <ircboost/irc.hpp>
#include <boost/asio/io_service.hpp>
#include <fmt/format.h>



struct MyClient : public ChatFuse::IRC::Client {
    bool connected = false;
    std::unordered_map<std::string, std::tuple<std::shared_ptr<LM::Inference>, size_t>> channels;
    std::optional<decltype(channels)::iterator> last_channel;
    bool nickserv_auth = false;
    std::string username, password;

    MyClient(boost::asio::io_context& io, const ChatFuse::IRC::Settings& settings = {})
        : ChatFuse::IRC::Client(io, settings)
    {}

    std::shared_ptr<LM::Inference> add_channel(const std::string& channel, bool final = true) {
        if (final) {
            LM::Inference::Params p;
            p.temp = 0.52f;
            p.prefer_mirostat = 2;
            p.n_repeat_last = 256;
            p.repeat_penalty = 1.20f;
            p.use_mlock = false;
            p.n_threads = 16;
            p.scroll_keep = 0.2f;
            std::shared_ptr<LM::Inference> ai;
            ai.reset(LM::Inference::construct("model.gguf", p));
            channels[channel] = {ai, 0};
            return ai;
        } else {
            channels[channel] = {nullptr, 0};
            return nullptr;
        }
    }

    void use_nickservauth(const std::string& username, const std::string& password) {
        nickserv_auth = true;
        this->username = username;
        this->password = password;
    }

    std::string_view nicknameFromSender(std::string_view sender) {
        return std::get<0>(ChatFuse::IRC::Parsing::splitOnce(sender, '!'));
    }

    boost::asio::awaitable<void> generate_reply(decltype(channels)::iterator channel_it) {
        fmt::print("Processing...\n");
        const auto& channel_name = channel_it->first;
        const auto& [ai, msg_count] = channel_it->second;
        // "Inspire" bot to respond here
        ai->append(fmt::format("<{}>", settings.name));
        try {
            // Let the AI do its stuff...
            auto message = ai->run("\n");
            // Strip ' ' from message
            if (message[0] == ' ') [[likely]] {
                message.erase(0, 1);
            }
            // Append to backlog
            ai->append(' ' + message + '\n');
            // Send message
            ChatFuse::IRC::Command cmd{
                .name = "PRIVMSG",
                .args = {std::string(channel_name)},
                .data = message
            };
            co_await sendCommand(cmd);
            // Log message
            logger.log(QLog::Loglevel::debug, fmt::format("Sending message to {}: {}", channel_name, message));
        } catch (std::exception& e) {
            logger.log(QLog::Loglevel::error, fmt::format("Error from api: {}", e.what()));
        }
    }

    boost::asio::awaitable<void> on_event_when_connected(ChatFuse::IRC::UEvent& event) {
         if (event->name == "PRIVMSG" && event->data[0] != '\x01') [[likely]] {
            logger.log(QLog::Loglevel::debug, fmt::format("Received message from {} in {}: {}", event->sender, event->receiver, event->data));
            fmt::print("<{}> {}\n", nicknameFromSender(event->sender), event->data);
            // Stop if there are no known channels
            if (channels.empty()) co_return;
            // If message is private, handle that differently
            if (event->receiver == settings.name) {
                // If message is force reply command, force reply
                if (event->data == "force reply") {
                    co_await generate_reply(last_channel.value_or(channels.begin()));
                } else if (event->data == "quit") {
                    ChatFuse::IRC::Command cmd{
                        .name = "QUIT",
                        .data = "leaving"
                    };
                    co_await sendCommand(cmd);
                } else if (event->data.starts_with("answer ")) {
                    // Get channel and message
                    auto channel = last_channel.value_or(channels.begin());
                    std::string message(event->data);
                    message.erase(0, 7);
                    // Send message
                    ChatFuse::IRC::Command cmd{
                        .name = "PRIVMSG",
                        .args = {channel->first},
                        .data = message
                    };
                    co_await sendCommand(cmd);
                    // Append message
                    std::get<0>(channel->second)->append(fmt::format("<{}> {}\n", settings.name, message));
                }
                // Stop doing anything else
                co_return;
            }
            // Find channel log
            auto res = channels.find(std::string(event->receiver));
            if (res != channels.end()) {
                // Append to backlog and increase message counter
                auto& [ai, msg_count] = res->second;
                ai->append(fmt::format("<{}> {}\n", nicknameFromSender(event->sender), event->data), [] (float progress) {
                    std::cout << progress << " %\n" << std::flush;
                    return true;
                });
                msg_count++;
                // If message contained name or "random" occasion and at least 4 messages were received, respond
                if (event->data.find(settings.name) != std::string_view::npos || (event->data.size() + msg_count) % 10 == 0) {
                    co_await generate_reply(res);
                    last_channel = res;
                }
            }
        } else if (event->name == "JOIN" && nicknameFromSender(event->sender) == settings.name) {
            logger.log(QLog::Loglevel::info, fmt::format("Joined {}", event->receiver));
            // Reset channels backlog
            add_channel(std::string(event->receiver));
        }
        co_return;
    }

    boost::asio::awaitable<void> onConnect() override {
        // Authenticate via nickserv
        if (nickserv_auth) {
            ChatFuse::IRC::Command cmd{
                .name = "PRIVMSG",
                .args = {"NickServ"},
                .data = fmt::format("IDENTIFY {} {}", username, password)
            };
            co_await sendCommand(cmd);
        }
    }

    virtual boost::asio::awaitable<void> onEvent(ChatFuse::IRC::UEvent event) override {
        if (connected) [[likely]] {
            co_await on_event_when_connected(event);
        } else {
            if (event->name == "MODE") {
                // Mark as "connected"
                connected = true;
                // Add some delay if nickserv is used
                if (nickserv_auth) {
                    co_await asyncSleep(10000);
                }
                // Join channels
                for (const auto& [channel_name, _] : channels) {
                    ChatFuse::IRC::Command cmd{
                        .name = "JOIN",
                        .args = {channel_name}
                    };
                    co_await sendCommand(cmd);
                }
            }
        }
    }
};
